#HW_L5_T2 Equals

1. Реализовать класс DigitHandler(int value).
Данный класс представляет собой обертку над числом value

* Реализовать equals & hashCode

2. Реализовать класс Person{name, city, age}, определить метод toString

* Реализовать equals & hashCode
* Гарантируется, что значения name, city не null
* Условие равенства: все поля name, city, age должны совпадать. (name и city без учета регистра)

###Критерии приемки

1. Предоставить Pull Request  из ветки feature/DigitHandlerEquality

Предоставить Pull Request  из ветки feature/PersonEquality

2. Публичные методы должны быть покрыты unit тестами